import Vue from 'vue'
import VModal from 'vue-js-modal'
import Vuex from 'vuex'
import VShowSlide from 'v-show-slide'
import Paginate from "vuejs-paginate";

import vSelect from 'vue-select'

Vue.component('v-select', vSelect)


Vue.component("paginate", Paginate);
Vue.use(VModal)
Vue.use(Vuex)
Vue.use(VShowSlide, {
  customEasing: {
    exampleEasing: 'ease'
  }
})

