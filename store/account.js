export const strict = false

export const state = () => ({
  logined: true,
})



export const mutations = {
  logout: (state) => {
    state.logined = false
  },
}
