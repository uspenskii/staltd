export default {
  server: {
    port: 8007,
    host: '0.0.0.0',
  },

  target: 'server',

  ssr: true,

  head: {
    title: 'staltd',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, height=device-height, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', href: '/icons/logo.svg' }
    ],
    bodyAttrs: {
      class: 'body',
    },
  },

  buildModules: ['nuxt-compress'],
  
  css: ['@/assets/scss/app.scss'],

  styleResources: {
    scss: ['@/assets/scss/variables.scss'],
  },

  plugins: [{ src: '~/plugins/plugins.js', mode: 'client' }, { src: '~/plugins/ymapPlugin.js',  mode: 'client' }, '~plugins/api.js'],

  components: true,

  modules: ['@nuxtjs/axios', '@nuxtjs/style-resources', '@nuxtjs/svg-sprite', 'nuxt-vue-select'],
  
  build: {    
    transpile: [ "three" ]
}
    
    
}
